package br.ueg.mobile.exitus.servico_cliente;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 5.5.0.6
//
// Created by Quasar Development 
//
//---------------------------------------------------



public class OperationResult< T>
{
    public java.lang.Exception Exception;
    public T Result;
    public java.lang.Object Tag;
    public java.lang.String MethodName;
}
